.PHONY: build run web
build:
	docker build -t react-app-me .
run:
	docker run -it --rm -p 3001:3000 -v ${PWD}:/usr/src/app -v /usr/src/app/node_modules -e CHOKIDAR_USEPOLLING=true --name react-app-me react-app-me
web:
	docker exec -it react-app-me sh
