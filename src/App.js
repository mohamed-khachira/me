import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import "./App.css";
import About from "./components/About";
import Contact from "./components/Contact";
import Education from "./components/Education";
import Experience from "./components/Experience";
import Sidebar from "./components/Sidebar";
import Skills from "./components/Skills";

function App() {
  return (
    <Router basename={"/me"}>
      <Sidebar></Sidebar>
      <div className="main-wrapper">
        <Switch>
          <Route path="/about" exact component={About} />
          <Route path="/education" exact component={Education} />
          <Route path="/experience" exact component={Experience} />
          <Route path="/skills" exact component={Skills} />
          <Route path="/contact" exact component={Contact} />
          <Route exact path="/">
            <Redirect to="/about" />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
