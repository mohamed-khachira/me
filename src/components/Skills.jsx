import React from "react";
import Progressbar from "./Progressbar";

function Skills() {
  return (
    <div>
      <section className="overview-section p-3 p-lg-5">
        <div className="container">
          <h2 className="section-title font-weight-bold mb-3">
            <i className="fas fa-laptop mr-2"></i>Technical Skills
          </h2>
          <div className="section-intro my-5"></div>
          <div className="box-gray">
            <Progressbar name="PHP" pct="90%" />
            <Progressbar name="Symfony" pct="80%" />
            <Progressbar name="Angular" pct="80%" />
            <Progressbar name="React" pct="60%" />
            <Progressbar name="MySQL" pct="80%" />
            <Progressbar name="Node.js" pct="60%" />
            <Progressbar name="DevOps" pct="50%" />
            <Progressbar name="Git" pct="60%" />
            <Progressbar name="HTML5" pct="80%" />
            <Progressbar name="CSS3" pct="80%" />
            <Progressbar name="Bootstrap" pct="80%" />
            <Progressbar name="JavaScript" pct="80%" />
            <Progressbar name="TypeScript" pct="70%" />
            <Progressbar name="Ionic" pct="50%" />
            <Progressbar name="Linux" pct="60%" />
          </div>
        </div>
      </section>
    </div>
  );
}

export default Skills;
