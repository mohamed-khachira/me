import React from "react";
import { NavLink } from "react-router-dom";

const Sidebar = () => {
  return (
    <header className="header text-center">
      <div className="force-overflow">
        <nav className="navbar navbar-expand-lg navbar-dark">
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navigation"
            aria-controls="navigation"
            aria-expanded="false"
            aria-label="Toggle navigation"
            style={{ zIndex: "2" }}
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div id="navigation" className="collapse navbar-collapse flex-column">
            <div className="profile-section pt-3 pt-lg-0">
              <img
                className="profile-image mb-3 rounded-circle mx-auto"
                src="images/circule-profile.png"
                alt=""
              />
              <h1 className="blog-name pt-lg-1 mb-0">
                <NavLink to="/about">Mohamed Khachira</NavLink>
              </h1>
              <div className="badge badge--gray my-3 mx-1">
                Full-Stack Software Engineer
              </div>
              <div className="email">
                <i className="fas fa-envelope-square mr-1"></i>
                mohamed.khachira@gmail.com
              </div>
              <hr />
            </div>
            <ul className="navbar-nav flex-column text-left">
              <li className="nav-item">
                <NavLink className="nav-link" to="/about">
                  <i className="fas fa-user  mr-1"></i> About
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/skills">
                  <i className="fas fa-laptop mr-1"></i>Skills
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/experience">
                  <i className="fas fa-business-time mr-1"></i>Experience
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/education">
                  <i className="fas fa-university mr-1"></i>Education
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/contact">
                  <i className="fas fa-envelope-open-text mr-1"></i>Contact
                </NavLink>
              </li>
            </ul>
            <ul className="social-list list-inline py-2 mx-auto">
              <li className="list-inline-item">
                <a
                  className="social__link"
                  href="https://www.linkedin.com/in/mohamedkhachira/"
                  target="_blank"
                  rel="noreferrer"
                >
                  <i className="fab fa-linkedin"></i>
                </a>
              </li>
              <li className="list-inline-item">
                <a
                  className="social__link"
                  href="https://gitlab.com/mohamed-khachira"
                  target="_blank"
                  rel="noreferrer"
                >
                  <i className="fab fa-gitlab"></i>
                </a>
              </li>
              <li className="list-inline-item">
                <a
                  className="social__link"
                  href="https://github.com/mohamed-khachira"
                  target="_blank"
                  rel="noreferrer"
                >
                  <i className="fab fa-github"></i>
                </a>
              </li>
              <li className="list-inline-item">
                <a
                  className="social__link"
                  href="https://twitter.com/medkhachira"
                  target="_blank"
                  rel="noreferrer"
                >
                  <i className="fab fa-twitter"></i>
                </a>
              </li>
            </ul>
            <div className="my-2">
              <a
                className="btn btn-primary"
                href="images/mohamed-khachira.pdf"
                target="_blank"
              >
                <i className="fas fa-download pr-2"></i>
                Download CV
              </a>
            </div>
            {/*
            <div className="dark-mode-toggle text-center w-100">
              <hr className="mb-4" />
              <h4 className="toggle-name mb-3 ">
                <i className="fas fa-adjust mr-1"></i>
                Dark Mode
              </h4>
              <input className="toggle" id="darkmode" type="checkbox" />
              <label className="toggle-btn mx-auto mb-0" htmlFor="darkmode" />
            </div>
          */}
          </div>
        </nav>
      </div>
    </header>
  );
};

export default Sidebar;
