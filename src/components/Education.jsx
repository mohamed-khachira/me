import React from "react";

function Education() {
  return (
    <div>
      <section className="overview-section p-3 p-lg-5">
        <div className="container">
          <h2 className="section-title font-weight-bold mb-3">
            <i className="fas fa-university mr-2"></i>Education
          </h2>
          <div className="section-intro my-5">
            <div className="timeline">
              <article className="timeline__item">
                <h5 className="title title--h3 timeline__title">
                  Higher Institute of Applied Science and Technology of Sousse
                  (Tunisia)
                </h5>
                <span className="timeline__period">2008 — 2013</span>
                <p className="timeline__description">
                  National Diploma of Engineer in Software Engineering.
                </p>
              </article>

              <article className="timeline__item">
                <h5 className="title title--h3 timeline__title">
                  Secondary School of Sidi Makhlouf, Medinine (Tunisia)
                </h5>
                <span className="timeline__period">2008</span>
                <p className="timeline__description">Baccalaureate degree in Computer Science</p>
              </article>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default Education;
