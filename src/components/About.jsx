import React from "react";

function About() {
  return (
    <div>
      <section className="overview-section p-3 p-lg-5">
        <div className="container">
          <h2 className="section-title font-weight-bold mb-3">
            <i className="fas fa-user mr-2"></i>About Me
          </h2>
          <div className="section-intro mb-5">
            I am a software engineer specialised in frontend and backend
            development for complex scalable web apps with a passion for coding
            and solving real-world problems using a modern tech stack (Symfony,
            Angular, React, Node, MongoDB, docker, etc...).
            <br /> I am looking forward to expanding my knowledge and developing
            new skills.
          </div>
        </div>
      </section>
      <section className="overview-section p-3 p-lg-5">
        <div className="container">
          <h2 className="section-title font-weight-bold mb-3">What I do?</h2>
          <div className="section-intro mb-5">
            I have 7 years experience as a software developer in IT companies,
            expert in Symfony and Angular Frameworks with extensive experience
            in the complete Software Development Life Cycle from gathering
            specifications, design, planning, coding, testing and deploying.
            Well-versed in numerous programming languages including HTML5, PHP,
            JavaScript, CSS3, MySQL...
          </div>
          <div className="row">
            <div className="col-12 col-lg-6 case-item-wrap">
              <div className="case-item">
                <i className="fas fa-file-code case-item__icon fa-3x"></i>
                <h3 className="title title--h3">Web Development</h3>
                <p className="case-item__caption">
                  High-quality development of sites at the professional level.
                </p>
              </div>
            </div>
            <div className="col-12 col-lg-6 case-item-wrap">
              <div className="case-item">
                <i className="fas fa-mobile-alt case-item__icon fa-3x"></i>
                <h3 className="title title--h3">Mobile Apps</h3>
                <p className="case-item__caption">
                  Professional development of applications for iOS and Android.
                </p>
              </div>
            </div>
            <div className="col-12 col-lg-6 case-item-wrap">
              <div className="case-item">
                <i className="fas fa-palette case-item__icon fa-3x"></i>
                <h3 className="title title--h3">Web Design</h3>
                <p className="case-item__caption">
                  The most modern and high-quality design made at a professional
                  level.
                </p>
              </div>
            </div>
            <div className="col-12 col-lg-6 case-item-wrap">
              <div className="case-item">
                <i className="fab fa-linux case-item__icon fa-3x"></i>
                <h3 className="title title--h3">DevOps</h3>
                <p className="case-item__caption">
                  Use of DevOps Tools in order to increase the speed of software
                  development and improve application scalability.
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default About;
