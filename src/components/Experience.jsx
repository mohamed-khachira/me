import React from "react";

function Experience() {
  const jobsList = [
    {
      jobTitle: "Senior Software Engineer",
      campanyLogo: "netskope.png",
      logoWidth: "165",
      logoheight: "34",
      logoAlt: "Netskope",
      period: "Oct 2021 - Present",
      role: "As a Senior Software Engineer, my mission is to design and develop high quality software services and features, working with other engineers and product managers. As well as improve Netskope products to ensure our customers' satisfaction.",
      tasks: [
        "Work in the frontend to implement the user interface.",
        "Work in the backend to develop web applications.",
        "Develop reusable UI components.",
        "Ensure that the Netskope web interface works seamlessly in all browsers.",
        "Keep abreast of the latest technologies and web methodologies which could be used in the Netskope product in future.",
      ],
    },
    {
      jobTitle: "Full Stack Web Developer | Symfony | Angular| Devops",
      campanyLogo: "logo-bnp.svg",
      logoWidth: "165",
      logoheight: "34",
      logoAlt: "BNP Paribas",
      period: "Nov 2018 - Oct 2021",
      role: "As a Software Development Leader, my mission is to provide a high quality and modern Web Applications. My responsibilities are primarily technical, I am the principal developer of our Symfony 4.4 / Angular7-based framework.",
      tasks: [
        "Took up the responsibility of the entire Project.",
        "Analysed requirements, designed and implemented new features.",
        "Implemented the Web SSO with Symfony.",
        "Designed and implemented RESTful services by using API Platform.",
        "Implemented the best practices and coding standards.",
        "Coached less experienced developers and newcomers.",
      ],
    },
    {
      jobTitle: "Senior Full-Stack Software Engineer",
      campanyLogo: "know&decide.png",
      logoWidth: "165",
      logoheight: "34",
      logoAlt: "Know &amp; Decide",
      period: "Aug.2014 — Sep.2018",
      role: "As a Front-End and Back-End Developer. I was involved in creating and maintaining dashboards in Capacity Planning Portal. My main tasks were to gather data, consolidate them and create a KPI (Key Performance Indicators) dashboard.",
      tasks: [
        "Created PHP jobs for data collection from multiple sources(csv, excel, xml, databases, etc…).",
        "Imported the collected data into the database.",
        "Designed and implemented RESTful services by using API Platform.",
        "Consolidated Data and built a KPI Dashboard using Amcharts.js for data visualisation.",
        "Created and implemented UI based on twig, Bootstrap and jQuery to customize dashboards.",
        "Maintained and supported the Capacity and Financial Management portal, including fixing bugs.",
      ],
    },
    {
      jobTitle: "Junior Software Engineer",
      campanyLogo: "accessleader.png",
      logoWidth: "",
      logoheight: "",
      logoAlt: "ACCÈS LEADER",
      period: "Oct.2013 — Jun.2014",
      role: "My mission was to rebuild the current back-office which was developed with CodeIgniter PHP Framework. Symfony 2 and Backbone.js were used for the developing of the new one",
      tasks: [
        "Integrated Symfony with Backbone.js.",
        "Created the database (MySQL) for the back-office.",
        "Integrated a Bootstrap Template into the project.",
        "loped two modules in the back-office which are : User management and Product management.",
        "Developed an Android application with Java and publish it on Google Play.",
      ],
    },
    {
      jobTitle: "Internship: Android Java Developer",
      campanyLogo: "st-logo.svg",
      logoWidth: "165",
      logoheight: "34",
      logoAlt: "STMicroelectronics",
      period: "Feb.2013 — Jul.2014",
      role: "My mission was to conduct a feasibility study of creating the mobile android application for a desktop application.",
      tasks: [
        "Created a mobile version from an existing desktop application(MicroExplorer) which describe Microcontroller.",
        "Published the application on google play.",
      ],
    },
  ];
  return (
    <div>
      <section className="overview-section p-3 p-lg-5">
        <div className="container">
          <h2 className="section-title font-weight-bold mb-3">
            <i className="fas fa-business-time mr-2"></i>Experience
          </h2>
          <div className="section-intro my-5">
            <div className="timeline">
              {jobsList.map((job, index) => (
                <article key={index} className="timeline__item">
                  <div className="flex-container">
                    <h5 className="title title--h3 timeline__title">
                      {job.jobTitle}
                    </h5>
                    <img
                      src={"images/" + job.campanyLogo}
                      width={job.logoWidth}
                      height={job.logoheight}
                      alt={job.logoAlt}
                      className="ml-5"
                    />
                  </div>
                  <span className="timeline__period"> {job.period}</span>
                  <div className="timeline__description">
                    {job.role}
                    <ul className="my-4">
                      {job.tasks.map((task, index) => (
                        <li key={index}>{task}</li>
                      ))}
                    </ul>
                  </div>
                </article>
              ))}
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default Experience;
