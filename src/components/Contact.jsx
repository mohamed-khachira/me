import React from "react";

function Contact() {
  return (
    <div>
      <section className="overview-section p-3 p-lg-5">
        <div className="container">
          <h2 className="section-title font-weight-bold mb-3">
            <i className="fas fa-envelope-open-text mr-2"></i>Contact details
          </h2>
          <div className="col-xs-12 col-sm-3 col-md-4 col-lg-4">
            <p>
              <i className="fas fa-map-marker"></i> Munich, Germany
            </p>
            <p>
              <i className="fas fa-mobile"></i> +49 15758515618
            </p>
            <p>
              <i className="fas fa-at mr-1"></i>
              <a href="mailto:mohamed.khachira@gmail.com">
                mohamed.khachira@gmail.com
              </a>
            </p>
            <p>
              <i className="fab fa-skype"></i> khachira.mohamed
            </p>
          </div>
        </div>
      </section>
    </div>
  );
}

export default Contact;
