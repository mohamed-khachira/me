import React from "react";

function Progressbar(props) {
  return (
    <div className="progress">
      <div
        className="progress-bar"
        role="progressbar"
        aria-valuenow="50"
        aria-valuemin="0"
        aria-valuemax="100"
        style={{ width: props.pct, zIndex: "2" }}
      >
        <div className="progress-text">
          <span>{props.name}</span>
          <span>{props.pct}</span>
        </div>
      </div>
      <div className="progress-text">
        <span>props.name</span>
      </div>
    </div>
  );
}

export default Progressbar;
