# My Awesome Portfolio using React

This is my online resume created using React and Bootstrap.

## Installation

To run this project follow these steps

```bash
1. Clone the repo using : git clone ...
2. Navigate to the folder using : cd ...
3. Run this command :  make build to prepare environment with docker
4. Run this command: make run to start the service on localhost:3001
```

App URL: https://mohamed-khachira.gitlab.io/me
